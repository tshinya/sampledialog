package sampledialog.component;

import javax.swing.JButton;

import sampledialog.action.ErrorDialogAdapter;


public class ErrorDialogJButton extends JButton {

	public ErrorDialogJButton() {
		super("Show error dialog");
		this.addActionListener(new ErrorDialogAdapter());
	}

}
