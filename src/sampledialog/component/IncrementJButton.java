package sampledialog.component;

import javax.swing.JButton;

import sampledialog.action.IncrementDialogAdapter;


public class IncrementJButton extends JButton {

	public IncrementJButton() {
		super("Increment");
		this.addActionListener(new IncrementDialogAdapter());
	}
	
}