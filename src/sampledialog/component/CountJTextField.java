package sampledialog.component;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import sampledialog.main.MainFrame;

public class CountJTextField extends JTextField {
	
	private int count;
	
	public CountJTextField() {
		super("0");
		this.count = 0;
	}
	
	public void increment() {
		this.count++;
		this.setText(String.valueOf(this.count));
	}
	
	public void decrement() {
		if (this.count > 0) {
			this.count--;
			this.setText(String.valueOf(this.count));
		} else {
			JOptionPane.showMessageDialog(MainFrame.getMainContentPane(), "これ以上減らすことは出来ません.", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

}
