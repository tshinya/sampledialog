package sampledialog.component;

import javax.swing.JButton;

import sampledialog.action.DecrementDialogAdapter;


public class DecrementJButton extends JButton {

	public DecrementJButton() {
		super("Decrement");
		this.addActionListener(new DecrementDialogAdapter());
	}
}
