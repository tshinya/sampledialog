package sampledialog.component;

import javax.swing.JButton;

import sampledialog.action.WarnigDialogAdapter;


public class WarningDialogJButton extends JButton {

	public WarningDialogJButton() {
		super("Show warning dialog");
		this.addActionListener(new WarnigDialogAdapter());
	}
}
