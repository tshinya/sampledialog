package sampledialog.main;

import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import sampledialog.action.CountActionManager;
import sampledialog.action.DialogWindowActionAdapter;
import sampledialog.component.CountJTextField;
import sampledialog.component.DecrementJButton;
import sampledialog.component.ErrorDialogJButton;
import sampledialog.component.IncrementJButton;
import sampledialog.component.WarningDialogJButton;


public class MainFrame extends JFrame {

	private static JPanel pane;

	public MainFrame() {
		super("DialogSampleApp");
		CountJTextField countTextField = new CountJTextField();
		new CountActionManager(countTextField);
		pane = (JPanel) getContentPane();
		pane.setLayout(new FlowLayout());
		pane.add(new ErrorDialogJButton());
		pane.add(new WarningDialogJButton());
		pane.add(new IncrementJButton());
		pane.add(new DecrementJButton());
		pane.add(countTextField);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new DialogWindowActionAdapter());
	}
	
	public static JPanel getMainContentPane() {
		return pane;
	}
}
