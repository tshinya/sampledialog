package sampledialog.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

import sampledialog.main.MainFrame;

public class IncrementDialogAdapter implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		int ans = JOptionPane.showConfirmDialog(MainFrame.getMainContentPane(), "Increment", "Increment", JOptionPane.YES_NO_OPTION);
		if (ans == JOptionPane.YES_OPTION) {
			CountActionManager.increment();
		}
	}

}