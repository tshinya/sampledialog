package sampledialog.action;

import sampledialog.component.CountJTextField;

public class CountActionManager {

	private static CountJTextField textField;
	
	public CountActionManager(CountJTextField _textField) {
		textField = _textField;
	}
	
	protected static void increment() {
		textField.increment();
	}
	
	protected static void decrement() {
		textField.decrement();
	}
}
