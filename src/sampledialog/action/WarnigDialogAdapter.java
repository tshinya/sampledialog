package sampledialog.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import sampledialog.main.MainFrame;


public class WarnigDialogAdapter implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		JOptionPane.showMessageDialog(MainFrame.getMainContentPane(), "Warning Message", "Warning", JOptionPane.WARNING_MESSAGE);
	}

}
