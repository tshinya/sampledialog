package sampledialog.action;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JOptionPane;

import sampledialog.main.MainFrame;

public class DialogWindowActionAdapter extends WindowAdapter {
	
	@Override
	public void windowClosing(WindowEvent e) {
		super.windowClosing(e);
		int ans = JOptionPane.showConfirmDialog(MainFrame.getMainContentPane(), "本当に終了しますか？", "Warning", JOptionPane.YES_NO_OPTION);
		if (ans == JOptionPane.YES_OPTION) {
			System.exit(0);
		}
	}

}