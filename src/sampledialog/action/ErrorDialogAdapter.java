package sampledialog.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

import sampledialog.main.MainFrame;


public class ErrorDialogAdapter implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		JOptionPane.showMessageDialog(MainFrame.getMainContentPane(), "Error Message", "Error", JOptionPane.ERROR_MESSAGE);
	}

}
